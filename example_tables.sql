CREATE TABLE `sgm_subscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_added` datetime DEFAULT NULL,
  `userid` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `country` varchar(2) DEFAULT NULL,
  `validation_status` int(11) DEFAULT '0',
  `subscription_status` varchar(10) DEFAULT NULL,
  `sgm_status` int(11) DEFAULT NULL,
  `sgm_track_id` int(11) DEFAULT NULL,
  `sgm_responce` varchar(255) DEFAULT NULL,
  `sgm_responce_http_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
)  DEFAULT CHARSET=utf8;

CREATE TABLE `ct_user` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `email` char(100) NOT NULL,
  `country` varchar(2) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `country_code` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`userid`)
)  DEFAULT CHARSET=utf8 ;