# SegTrack publisher API demo application#

### API documentation
read our [Wiki](https://bitbucket.org/segmobvlz/publisher-app/wiki/Home)
### This is the demo application to publish leads into SegTrack system ###
This _Demo_ application shows how to push leads to SegTrack server 
It is supposed to be run via cron job.
Main purpose of this application to give you base structure for building your own well-tuned application



### setup ###
#### database structure ####
*This app relies on database tables defined in `exampe_tables.sql` file.
Fill free end edit application code*


* clone  application 

```shell
git clone https://vladzaitsev@bitbucket.org/segmobvlz/publisher-app.git
```

* Change the current working directory to your cloned repository and install dependencies. 
```shell
cd publisher-app
composer install
```

* create config file 
```shell 
cp config/config.php.example connfig/config.php
```
* edit `config/config.php`: set database connection credentials  and API key

### running ###
*Review code, edit as you need*

to run in console 
```command-line
cd [path to application]
php app.php
```