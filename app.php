<?php
/**
 * Created by PhpStorm.
 * User: VLZ
 * Date: 26.10.2016
 * Time: 21:27
 */
error_reporting(E_ALL);
use SGMPublisher\Emsgd;
use SGMPublisher\SGMAPI;
use SGMPublisher\SGMUsers;

require __DIR__ . '/vendor/autoload.php';
$config = require __DIR__ . '/config/config.php';


/**
 * Configure HTTP client for SegMob API interaction
 */
$http_client = new \GuzzleHttp\Client([
    'base_uri' => $config['api_uri'],
    'timeout'  => 2.0,
    'headers'  => [
        'SG-API-KEY'   => $config['SG_API_KEY'],
        'Content-Type' => 'application/json'
    ]
]);

/**
 * Inject client to API
 */
$api = new SGMAPI($http_client);

try {
    $dbh = new PDO(
        "mysql:host=" . $config['db_hostname'] . ";dbname=" . $config['db_name'],
        $config['db_username'],
        $config['db_password']
//        array(PDO::ATTR_PERSISTENT => TRUE)
    );
    $dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
    $dbh->setAttribute(PDO::ATTR_AUTOCOMMIT, 1);
    $dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, FALSE);
} catch (PDOException $e) {
    die ("Could not connect to database");
}

$users = new SGMUsers($dbh);

/**
 * get 10 leads older than 5 days
 */

$new_users = $users->get_new_users(10, 5);

if ($new_users) {
    foreach ($new_users as $u) {

        /**
         * In our example we have concatenated name
         * split full name into the first and last name
         */
        list($u->first_name, $u->last_name) = $api->split_name($u->username);
        /**
         *  add user to the tracking table with
         *  status 'pending' and validation flag
         *  so  in next round we not add it again
         *  and send only valid phones
         */
        try {
            list ($u->phone, $u->country) = $api->validate_phone($u->phone, $u->country);;
            $valid = 1;
        } catch (UnexpectedValueException $e) {
            $valid = 0;
        }
        $subscription_id = $users->add_user_subscription($u, $valid);

    }
}
/**
 *  check statuses of the pending leads
 */
$pending_leads = $users->get_pending_leads(5);
if ($pending_leads) {
    foreach ($pending_leads as $u) {

        if ($u->subscription_status == 'pending') {
            /**
             * send  pending lead to SegMob
             */
            list($http_code, $body) = $api->post_lead($u->first_name, $u->last_name, $u->phone, $u->email, $u->country);
            $track_id = null;
            switch ($http_code) {
                case 400: //lead could not be registered
                    $subscription_status = 'denied';
                    break;
                case 200: //lead added
                    $track_id = $body->lead_id;

                    $subscription_status = $track_id ? 'accepted' : 'error';
                    break;
                default: //some auth or server error, try again in next round
                    $subscription_status = 'pending';
            }
            /**
             * save latest response and new status
             */
            $users->update_sgm_status($u->id, $http_code, $body->message, $subscription_status, $track_id);

        } elseif ($u->subscription_status == 'accepted') {
            /**
             *  Check lead status
             */
            list($http_code, $body) = $api->get_lead_status($u->sgm_track_id);
            if ($http_code === 200) {
                /**
                 * set sgm status to 1 - accepted , or 0 - declined. if status is null,
                 *  lead is in moderation list, check again in next round
                 */
                $users->update_lead_status($u->id, $body->status);
            }
            /**
             * save latest response
             */
            $users->update_sgm_status($u->id, $http_code, $body->message, $u->subscription_status, $u->sgm_track_id);
        }
        /**
         * to prevent blocking by DDoS protection or IDS systems
         * send request one a second
         */
        sleep(1);
    }
}