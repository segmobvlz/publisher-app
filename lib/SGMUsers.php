<?php
/**
 * Created by PhpStorm.
 * User: VLZ
 * Date: 26.10.2016
 * Time: 22:08
 */

namespace SGMPublisher;


use PDO;

class SGMUsers
{
    /** @var  PDO */
    private $dbh;
    /** @var  \PDOStatement */
    private $add_lead_stmt;
    /** @var  \PDOStatement */
    private $update_sgm_status_stmt;
    /** @var  \PDOStatement */
    private $update_lead_status_stmt;

    public function __construct($database)
    {
        $this->dbh = $database;
        $this->add_lead_stmt = $this->dbh->prepare("
            INSERT INTO sgm_subscription
            (userid,first_name,last_name,email,phone,country,validation_status,date_added,subscription_status)
            VALUES(:userid,:first_name,:last_name,:email,:phone,:country,:validation_status,now(),'pending')
        ");
        $this->update_sgm_status_stmt = $this->dbh->prepare("
            UPDATE sgm_subscription SET 
              sgm_responce_http_status = :http_code,
              sgm_responce = :message,
              sgm_track_id = :sgm_track_id,
              subscription_status = :subscription_status
              WHERE id = :id 
        ");
        $this->update_lead_status_stmt = $this->dbh->prepare("
            UPDATE sgm_subscription SET 
              sgm_status = :sgm_status
              WHERE id = :id 
        ");

    }

    public function update_sgm_status($subscription_id, $http_code, $message, $subscription_status, $track_id)
    {
        $this->update_sgm_status_stmt->bindParam(':id', $subscription_id, PDO::PARAM_INT);
        $this->update_sgm_status_stmt->bindParam(':http_code', $http_code, PDO::PARAM_INT);
        $this->update_sgm_status_stmt->bindParam(':message', $message, PDO::PARAM_STR);
        $this->update_sgm_status_stmt->bindParam(':subscription_status', $subscription_status, PDO::PARAM_STR);
        $this->update_sgm_status_stmt->bindParam(':sgm_track_id', $track_id, PDO::PARAM_INT);
        $this->update_sgm_status_stmt->execute();
    }

    public function add_user_subscription($user, $validation_status)
    {
        $this->add_lead_stmt->bindParam(':userid', $user->userid, PDO::PARAM_INT);
        $this->add_lead_stmt->bindParam(':first_name', $user->first_name, PDO::PARAM_STR);
        $this->add_lead_stmt->bindParam(':last_name', $user->last_name, PDO::PARAM_STR);
        $this->add_lead_stmt->bindParam(':email', $user->email, PDO::PARAM_STR);
        $this->add_lead_stmt->bindParam(':phone', $user->phone, PDO::PARAM_STR);
        $this->add_lead_stmt->bindParam(':country', $user->country, PDO::PARAM_STR);
        $this->add_lead_stmt->bindParam(':validation_status', $validation_status, PDO::PARAM_INT);
        $r = $this->add_lead_stmt->execute();
        if (!$r) {
            return 0;
        }
        return $this->dbh->lastInsertId();
    }

    public function update_lead_status($subscription_id, $sgm_status)
    {
        $this->update_lead_status_stmt->bindParam(':sgm_status', $sgm_status, PDO::PARAM_INT);
        $this->update_lead_status_stmt->bindParam(':id', $subscription_id, PDO::PARAM_INT);
        $this->update_lead_status_stmt->execute();
    }

    public function get_pending_leads($lim1t)
    {
        $sSQL = "
            SELECT id, email, phone,first_name,last_name,country,subscription_status,sgm_track_id
              FROM sgm_subscription
              WHERE  validation_status = 1 AND 
              (subscription_status =  'pending' -- not saved to segmob 
                OR subscription_status =  'accepted' AND sgm_status IS NULL -- or awaiting moderation on segmob
                )
                ORDER BY date_added
                LIMIT " . (int)$lim1t . "
        ";
        $stmt = $this->dbh->prepare($sSQL);
        $stmt->execute();
//        Emsgd::p($stmt->queryString);
        return $stmt->fetchAll();
    }

    public function get_new_users($limit, $older_then)
    {
        $sSQL = "
        SELECT u.userid, u.email, u.phone,u.username,u.country 
        FROM ct_user u
        LEFT OUTER JOIN sgm_subscription s ON (s.userid = u.userid) 
         WHERE 1=1 
         AND created_at < now() - INTERVAL " . (int)$older_then . " DAY
         and created_at > now() - interval 30+".(int)$older_then." day
         AND s.id IS NULL
         ORDER BY created_at DESC 
        LIMIT " . (int)$limit . "
        
        ";
        $stmt = $this->dbh->prepare($sSQL);
        $stmt->execute();
//        Emsgd::p($stmt->queryString);
        return $stmt->fetchAll();

    }
}