<?php
/**
 * Created by PhpStorm.
 * User: VLZ
 * Date: 26.10.2016
 * Time: 21:40
 */

namespace SGMPublisher;


use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use UnexpectedValueException;

class SGMAPI
{
    /** @var  \GuzzleHttp\Client */
    private $client;
    private $phoneUtil;

    public function __construct($client)
    {
        $this->client = $client;
        $this->phoneUtil = PhoneNumberUtil::getInstance();
    }

    public function get_lead_status($lead_id)
    {
        $response = $this->client->request('get', 'lead/' . (int)$lead_id, [
            'http_errors' => FALSE,
            //            'debug'=>true,
        ]);
        $http_code = $response->getStatusCode();
        try {
            $body = json_decode((string)$response->getBody());
        } catch (\Exception $e) {
            $body = "Unknown server response";
        }

        return [$http_code, $body];
    }

    public function post_lead($fname, $lname, $phone, $email, $country)
    {
        $body = [
            'first_name'        => $fname,
            'last_name'         => $lname,
            'real_phone_number' => $phone,
            'email'             => $email,
            'country'           => $country,
        ];
        $response = $this->client->request('POST', 'lead', [
            'json'        => $body,
            'http_errors' => FALSE,
            //           'debug'=>true,

        ]);
        $http_code = $response->getStatusCode();
        try {
            $body = json_decode((string)$response->getBody());
        } catch (\Exception $e) {
            $body = "Unknown server response";
        }

        return [$http_code, $body];
    }

    public function validate_phone($phone, $country)
    {
        if (empty($phone)) {
            throw new UnexpectedValueException("Phone is required");
        }
        try {
            $phone = $this->phoneUtil->parse($phone, $country);
        } catch (NumberParseException $e) {
            throw new UnexpectedValueException("Phone is invalid for this country");
        }

        if (!$this->phoneUtil->isValidNumber($phone)) {
            throw new UnexpectedValueException("Phone is invalid");
        };
        return [$this->phoneUtil->format($phone, PhoneNumberFormat::E164),
                $this->phoneUtil->getRegionCodeForNumber($phone)
        ];
    }

    public function split_name($name)
    {
        $name = trim($name);
        $last_name = (strpos($name, ' ') === FALSE) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $first_name = trim(preg_replace('#' . $last_name . '#', '', $name));
        return array($first_name, $last_name);
    }

}